package mediatheque1cxf;

import java.util.Date;

import javax.jws.WebService;

@WebService
public interface ILivresService {
	public String getInfos();
	public boolean estEmpruntable(int id);
	public Date getRetour(int id);
	public Livre getLivreDuMois();
	public Livre[] getLivresDeLannee();
}

