package mediatheque1cxf;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class ClientMain {

	public static void main(String[] args) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres");
		factory.setServiceClass(ILivresService.class);
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add(new LoggingInInterceptor());
		ILivresService livresServices = factory.create(ILivresService.class);
		System.out.println(livresServices.getInfos());
		System.out.println("Livre 4 empruntable : "+livresServices.estEmpruntable(4));
		try {
		System.out.println("Livre -3 empruntable : "+livresServices.estEmpruntable(-3));
		} catch(Exception ex) {
			System.out.println("Exception : "+ex.getMessage());
		}
		System.out.println("Retour du livre 4 : "+livresServices.getRetour(4));
		System.out.println("Livre du mois : " +livresServices.getLivreDuMois());
		Livre[] la = livresServices.getLivresDeLannee();
		System.out.println("Livre de mars : "+la[2].getTitre());
	}	

}
