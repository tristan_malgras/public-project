package mediatheque2spring;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ClientSpringMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java Spring");
			RestTemplate rt = new RestTemplate();
			String health = rt.getForObject("http://localhost:9002/health", String.class);
			System.out.println("Health : " + health);
			Integer nbCd = rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);
			System.out.println("Il y a " + nbCd + " cd à la médiathèque");
			Cd cd1 = new Cd("Abbey Road", "The Beatles", 1966);
			rt.postForObject("http://localhost:9002/cd", cd1, Void.class);

			ParameterizedTypeReference<List<Cd>> ref = new ParameterizedTypeReference<List<Cd>>() {
			};
			ResponseEntity<List<Cd>> cdsEntity = rt.exchange("http://localhost:9002/cd", HttpMethod.GET, null, ref);
			List<Cd> cds = cdsEntity.getBody();
			for (Cd cd : cds) {
				System.out.println(" - " + cd);
			}
			
			Cd cd0 = rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			System.out.println("Premier CD :"+cd0);
			
			System.out.println("Modification titre");
			rt.put("http://localhost:9002/cd/0/titre","Help!", String.class);

		} catch (Exception ex) {
			System.err.println("Erreur : " + ex);
		}
	}

}
