package mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.Remote;

@Remote
public interface IDvdtheque {
	public String getInfos();
	public boolean ouvertA(LocalTime t);
	public LocalTime getDerniereInterrogation();

}
