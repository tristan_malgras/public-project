package mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import mediatheque3ejb.Dvd;
import mediatheque3ejb.IDvdDAO;
import mediatheque3ejb.IDvdtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("Dvd : client lourd");
		try {
			Context context = new InitialContext();
			IDvdtheque dvdtheque = (IDvdtheque) context.lookup(
					"ejb:/Mediatheque3Ejb/Dvds!mediatheque3ejb.IDvdtheque");
			System.out.println("Informations :"+dvdtheque.getInfos());
			System.out.println("Ouvert à 17h30 ? "+dvdtheque.ouvertA(LocalTime.of(17, 30)));			
			// Ajouter des DVDs en utilisant le DvdDAO
			IDvdDAO dvdDAO = (IDvdDAO) context.lookup(
					"ejb:/Mediatheque3Ejb/DvdDAO!mediatheque3ejb.IDvdDAO");
			dvdDAO.ajouter(new Dvd("Gladiator",2002));
			dvdDAO.ajouter(new Dvd("Toy Story",2001));
			System.out.println("Il y a maintenant : "+dvdDAO.getNombre());
			
			context.close();
		} catch (Exception ex) {
			System.err.println(ex);
		}
	
	}

}
