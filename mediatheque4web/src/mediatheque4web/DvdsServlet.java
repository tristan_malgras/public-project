package mediatheque4web;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalTime;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mediatheque3ejb.Dvd;
import mediatheque3ejb.IDvdDAO;
import mediatheque3ejb.IDvdtheque;
@WebServlet("/dvds")
public class DvdsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IDvdtheque dvdtheque;
	private IDvdDAO dvdDAO;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws 
		ServletException, IOException {
		resp.setContentType("text/html");
		
		Writer out = resp.getWriter();
		out.write("<!DOCTYPE html><html><body>");
		out.write("<h1>DVDthèque</h1>");
		out.write("<p>"+dvdtheque.getInfos()+"</p>");
		out.write("<p>Ouvert à 9h ? "+dvdtheque.ouvertA(LocalTime.of(9,0))+"</p>");
		out.write("<p>Quand ? "+dvdtheque.getDerniereInterrogation()+"</p>");
		//Afficher la table des DVD
		out.write("<table><tr><th>Titre</th><th>Année</th></tr>");
		for(Dvd dvd : dvdDAO.lireTous()) {
			out.write("<tr><td>"+dvd.getTitre()+"</td>"+
					"<td>"+dvd.getAnnee()+"</td><tr/>");
		}
		out.write("</table>");
		out.close();
	}
	@EJB(lookup="ejb:/Mediatheque3Ejb/Dvds!mediatheque3ejb.IDvdtheque")
	public void setDvdtheque(IDvdtheque dvdtheque) {
		this.dvdtheque = dvdtheque;
	}
	
	@EJB(lookup="ejb:/Mediatheque3Ejb/DvdDAO!mediatheque3ejb.IDvdDAO")
	public void setDvdDAO(IDvdDAO dvdDAO) {
		this.dvdDAO = dvdDAO;
	}

	
	

}
